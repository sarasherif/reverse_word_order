
const reverseWordOrder = (sentence) =>{
    return sentence.split(/\b/).reverse().join('');
}

const reversedSentence = reverseWordOrder('​Mary had a little lamb.');
console.log('Reversed word order of sentence:', reversedSentence);
